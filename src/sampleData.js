import axios from 'axios';

async function sampleFunc() {
  let { data, status } = await axios.get('http://' + window.location.hostname + ":3000" + '/samples_state')
  
  console.log('/samples_state status ', status);

  return  data;
}

export default sampleFunc;
