import { AddressbarColor } from 'quasar'

export default () => {
  AddressbarColor.set('#80b3ff')
}
