import HTTP from '../../services/api/http-common';

const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';
const auth = {
  state() {
    return {
      authenticated: false,
      userId: null,
      token: null,
    };
  },
  mutations: {
    [LOGIN](state, res) {
      const localStorage = state;
      localStorage.token = res.token;
      localStorage.userId = res.id;
      localStorage.authenticated = true;
    },

    [LOGOUT](state) {
      const localStorage = state;
      localStorage.token = null;
      localStorage.userId = null;
      localStorage.authenticated = false;
    },
  },
  actions: {
    login({ commit }, credentials) {
      return HTTP.post('login', {
        username: credentials.username,
        password: credentials.password,
      })
        .then((response) => {
          const res = {
            token: response.data.token,
            id: response.data.user_id,
          };

          commit(LOGIN, res);
        });
    },

    logout({ commit }) {
      commit(LOGOUT);
    },
  },
};

export default auth;
