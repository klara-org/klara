export default {
  Dessalinizadores: {
    "type": "FeatureCollection",

    "features": [
      { "type": "Feature", "properties": 
        { "Name": "AC", "Description": "ACRE \/ NORTE" }, "geometry": 
          { "type": "LineString", "coordinates": [ [-11.13855, -44.01812] ] } },
      { "type": "Feature", "properties": 
        { "Name": "AC", "Description": "ACRE \/ NORTE" }, "geometry": 
          { "type": "LineString", "coordinates": [ [-4.50334, -45.82878] ] } }
    ]
  }
};